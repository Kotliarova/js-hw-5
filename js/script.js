// Опишіть своїми словами, що таке метод об'єкту
//Це функція яка належить об'єкту. Тобто вона створюється в середені об'єкта та здійснює дії(операції) тільки з даними які є всередені об'єкта.

// Який тип даних може мати значення властивості об'єкта?
// будь-який тип данних (number, string, boolean, object, array...)

// Об'єкт це посилальний тип даних. Що означає це поняття?
// Це означає що якщо я захочу скопіювати об'єкт и напишу наприклад:
// const obj = {};
// const obj2 = obj
// тоді я не зроблю копію данних які місятаться в obj, а тільки зроблю посилання на obj. В цьому випадку якщо ми викличемо obj2 ми просто перейдемо на obj, а він в свою чергу надасть дані які в ньому містяться. Тому він і називається посилальний тип данних. Щоб скопіювати данні обєкта треба скористатися Object.assign


function createNewUser() {
    const userFirstName = prompt('Enter your name');
    const userLastName = prompt('Enter your last name');
    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,
        getLogin: function() {
            return (newUser.firstName.slice(0,1) + newUser.lastName).toLowerCase();
        },
        setfirstName: function(firstName) {
            this.firstName = firstName;
        },
        setlastName: function(lastName) {
            this.lasttName = lastName;
        }
    }
    return newUser;
}
let user = createNewUser();
console.log(user);
console.log(user.getLogin());

//варіант 2, але є проблема (залишила поруч комент)

// function createNewUser() {
//     const userFirstName = prompt('Enter your name');
//     const userLastName = prompt('Enter your last name');
//     const newUser = {};
//     Object.defineProperty(newUser, 'firstName', {
//         value: userFirstName,
//         writable: false
//     });
//     Object.defineProperty(newUser, 'lastName', {
//         value: userLastName,
//         writable: false
//     });
//     Object.defineProperty(newUser, 'getLogin', {
//         get: function() {
//             return (this.firstName.slice(0,1) + this.lastName).toLowerCase();
//         }
//     });
//     Object.defineProperty(newUser, 'setfirstName', {
//         set: function(firstName) {
//             this.firstName = firstName
//         }
//     });
//     Object.defineProperty(newUser, 'setlastName', {
//         set: function(lastName) {
//             this.lastName = lastName
//         }
//     });
//     return newUser;
// }
// let user = createNewUser();
// console.log(user);
// console.log(user.getLogin()); // TypeError: user.getLogin is not a function не розумію чому???

